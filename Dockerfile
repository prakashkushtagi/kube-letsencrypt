FROM alpine:3.12

RUN apk add --no-cache certbot python3
RUN mkdir /etc/letsencrypt

COPY secret-patch-template.json /
COPY entrypoint.sh /

CMD ["/entrypoint.sh"]
