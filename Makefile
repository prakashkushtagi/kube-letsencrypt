.PHONY: build push

build:
	docker build -t registry.gitlab.com/scvalex/kube-letsencrypt:master .

push:
	docker push registry.gitlab.com/scvalex/kube-letsencrypt:master
